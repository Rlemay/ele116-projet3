package view;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.ImageOutputStream;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;
import java.awt.event.*;

import controller.*;
import model.*;
import model.Image;

public class MainFen extends Fenetre implements ObserverIF {
	
	public static BufferedImage imageEnInput;

	private static PreviewFen prevFen = new PreviewFen(0, 0, "Lab3 - Image preview");
	private static StatsFen statFen = new StatsFen(500, 400, "Lab3 - modification tracker");
	
	private static DisplayCentral canvas = new DisplayCentral();
	
	private static int offsetX = 0;
	private static int offsetY = 0;
	
	private static int milieuOriginalX = 0;
	private static int milieuOriginalY = 0;

	JPanel centre = new JPanel();
	JPanel toolBar = new JPanel();
	JPanel zoneTravail = new JPanel();
	
	JMenuBar menuBar = new JMenuBar();
		JMenu mFichier = new JMenu("Fichier");
			JMenuItem miCharger = new JMenuItem("Charger une image");
			JMenuItem miSave = new JMenuItem("Enregistrer");
			JMenuItem miFermer = new JMenuItem("Fermer");
			
		JMenu mEditer = new JMenu("Editer");
			JMenuItem miUndo  = new JMenuItem("Retour arri�re");
			JMenuItem miRedo = new JMenuItem("Retour avant");
	
		JMenu mApropos = new JMenu("� propos");
			JMenuItem miAuteurs = new JMenuItem("Auteurs");
	
	JButton upButton = new JButton("Translate haut");
	JButton downButton = new JButton("Translate bas");
	JButton leftButton = new JButton("Translate gauche");
	JButton rightButton = new JButton("Translate droite");
	
	JButton zoomIN = new JButton("Zoom in");
	JButton zoomOUT = new JButton("Zoom out");
	
	public MainFen(int hauteur, int largeur, String titre) {
		
		super(hauteur, largeur, titre);
		
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setResizable(false);
		
		menuBar.add(mFichier);
			mFichier.add(miCharger);
			mFichier.add(miSave);
			mFichier.add(miFermer);
		
		menuBar.add(mEditer);
			mEditer.add(miUndo);
			mEditer.add(miRedo);
		
		menuBar.add(mApropos);
			mApropos.add(miAuteurs);
		
		toolBar.add(upButton);
		toolBar.add(downButton);
		toolBar.add(leftButton);
		toolBar.add(rightButton);
		toolBar.add(zoomIN);
		toolBar.add(zoomOUT);
		
		miCharger.addActionListener(new LoadMenuItem());
		miSave.addActionListener(new SaveMenuItem());
		miFermer.addActionListener(new QuitMenuItem());
		
		miUndo.addActionListener(new UndoMenuItem());
		miRedo.addActionListener(new RedoMenuItem());
		
		miAuteurs.addActionListener(new AuteursMenuItem());
			
		upButton.addActionListener(new UpButton());
		downButton.addActionListener(new DownButton());
		leftButton.addActionListener(new LeftButton());
		rightButton.addActionListener(new RightButton());
		
		zoomIN.addActionListener(new ZoomInButton());
		zoomOUT.addActionListener(new ZoomOutButton());
		
		zoneTravail.setLayout(new BorderLayout());
		zoneTravail.add(canvas, BorderLayout.CENTER);
		
		JScrollPane scrollPane = new JScrollPane(zoneTravail);
		zoneTravail.setBackground(Color.WHITE);

		this.setLocationRelativeTo(null);	
		this.setLayout(new BorderLayout());
		this.add(menuBar, BorderLayout.NORTH);
		this.add(centre, BorderLayout.CENTER);
		
		centre.setLayout(new BorderLayout());
		toolBar.setLayout(new FlowLayout());
		centre.add(toolBar, BorderLayout.NORTH);
		centre.add(scrollPane, BorderLayout.CENTER);
		
		scrollPane.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, Color.gray));

		scrollPane.setLayout(null);
		scrollPane.setSize(getMaximumSize());
		
		this.setVisible(true);

		milieuOriginalX = (int) (canvas.getSize().getWidth() / 2);
		milieuOriginalY = (int) (canvas.getSize().getHeight() / 2);
			
		Image.getInstance().addObserver(this);
			
	}
	
	private static class DisplayCentral extends JLabel{
		
		private BufferedImage img = null;
		
		protected DisplayCentral(){
			
			this.setBackground(Color.WHITE);
			this.setOpaque(true);
			
		}
			
	}
	
	public static File loadMethod(){
		
		int returnVal = 0;
		
		File loadedFile = new File("LoadedFile");
		JFileChooser fileChooser = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("JPG and PNG images only", "jpg", "png");
		fileChooser.setFileFilter(filter);
		
		int hauteurImage = 0;
		int largeurImage = 0;
		
		returnVal = fileChooser.showOpenDialog(new JFrame());
		
		if(returnVal == JFileChooser.APPROVE_OPTION){
			
			loadedFile = fileChooser.getSelectedFile();
			
			try{
				
				imageEnInput = ImageIO.read(loadedFile);
				
			}
			catch(Exception e){
				
				e.printStackTrace();
				
			}
			
			hauteurImage = imageEnInput.getHeight();
			largeurImage = imageEnInput.getWidth();
			
			prevFen.afficheImage(hauteurImage, largeurImage, imageEnInput);
			statFen.updateDisplay(0, 0, hauteurImage, largeurImage, 0, 1);
			prevFen.setVisible(true);
			statFen.setVisible(true);
					
		}
		
		return loadedFile;
		
	}
	
	public void update(BufferedImage img) {
		
		int largeurImg;
		int hauteurImg;
		
		canvas.setIcon(new ImageIcon(img));
		
		largeurImg = img.getWidth();
		hauteurImg = img.getHeight();
		
		canvas.setSize(largeurImg, hauteurImg);
		
		canvas.setLocation(milieuOriginalX - (int)(largeurImg/2) + offsetX,  
						   milieuOriginalY - (int)(hauteurImg/2) + offsetY);
		
	}
	
	public static File saveMethod(){
		
		JFileChooser fileSaver = new JFileChooser();
		FileNameExtensionFilter saveFilter = new FileNameExtensionFilter("JPEG only", "jpg");
		fileSaver.setFileFilter(saveFilter);
		File f = null;
		
		if(fileSaver.showSaveDialog(new JFrame()) == fileSaver.APPROVE_OPTION){
			
			return f = new File(fileSaver.getSelectedFile().getAbsolutePath() + ".jpg");
			
		}
		
		return null;
		
	}
		
	public static void modifTranslation(int transX, int transY){
		
		MainFen.offsetX = MainFen.offsetX + transX;
		MainFen.offsetY = MainFen.offsetY + transY;
		
	}

}
