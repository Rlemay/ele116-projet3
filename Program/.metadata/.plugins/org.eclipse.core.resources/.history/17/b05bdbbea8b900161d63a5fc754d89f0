package model;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import controller.*;

public class Image implements ObservableIF {
	
	private static Image instance = new Image();
	
	private BufferedImage imageModele;
	private File path;
	private int largeur;
	private int hauteur;
	private int posX;
	private int posY;
	private int echelle;
	
	private List<ObserverIF> listObservers = new ArrayList<ObserverIF>();
	
	private Image(){
	}
	
	public static Image getInstance(){
		return instance;
		
	}
	
	public BufferedImage getImage(){
		return this.imageModele;
	}
	
	public File getPath(){
		return this.path;
	}
	
	public int getLargeur(){
		return this.largeur;
	}
	
	public int getHauteur(){
		return this.hauteur;
	}
	
	public int getPosX(){
		return this.posX;
	}
	
	public int getPosY(){
		return this.posY;
	}
	
	public int getEchelle(){
		return this.echelle;
	}
	
	public void setPath(File path){
		this.path = path;
	}
	
	public void setLargeur(int largeur){
		this.largeur = largeur;
	}
	
	public void setHauteur(int hauteur){
		this.hauteur = hauteur;
	}
	
	public void setPosX(int posx){
		this.posX = posx;
	}
	
	public void setPosY(int posy){
		this.posY = posy;
	}
	
	public void setEchelle(int echelle){
		this.echelle = echelle;
	}
	
	public void setImage(BufferedImage img){
		
		this.imageModele = img;
		
	}
	
	public void saveImage(File file){
				
		int h = imageModele.getHeight();
		int l = imageModele.getWidth();
		
		BufferedImage imgToSave = new BufferedImage(l, h, BufferedImage.TYPE_INT_RGB);
		
		int[] rgb = imageModele.getRGB(0, 0, l, h, null, 0, l);
		imgToSave.setRGB(0, 0, l, h, rgb, 0, l);
		
		try {
			
			ImageIO.write(imgToSave, "jpg", file);
		
		} catch (IOException e) {
		
			e.printStackTrace();
		
		}
		
	}
	
	public void loadImage(File file){
		
		try {
			
			this.imageModele = ImageIO.read(file);
			this.notifyObservers();
		
		} catch (IOException e) {
		
			e.printStackTrace();
		
		}
		
	}

	public static BufferedImage imageToBI(Image img){
		
	    // Create a buffered image with transparency
	    BufferedImage bimage = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_INT_ARGB);

	    // Draw the image on to the buffered image
	    Graphics2D bGr = bimage.createGraphics();
	    bGr.drawImage(img, 0, 0, null);
	    bGr.dispose();

	    // Return the buffered image
	    return bimage;
		
	}
	
	public void addObserver(ObserverIF obs) {
		
		listObservers.add(obs);
	
	}

	public void removeObserver(ObserverIF obs) {
		
		listObservers.remove(obs);
		
	}

	public void notifyObservers() {
		
		for(ObserverIF observer : listObservers){
			
			observer.update(this.getImage());

		}
		
	}
	
}

