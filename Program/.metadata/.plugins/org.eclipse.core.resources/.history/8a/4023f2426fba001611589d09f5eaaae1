/************************************************************************************************
Cours: ELE116
Session : A2016
Groupe: 01
Projet: Laboratoire #3
�tudiant(e)(s) : Olivier Goyette
				 Marc-Andr� Pineault
				 Rapha�l Lemay
				 Fran�ois Doskas-Lambert
				 
Code(s) perm. : GOYO20049004
				PINM07019101
				LEMR17129205
				DOSF16049108
				
Nom du fichier: Image.java
Description du fichier: Classe contenant le singleton du mod�le de donn�e. Est le sujet
de l'observer, notify le MainFen en cas de changement.
Date cr��: 2016-12-04
*************************************************************************************************/

package model;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import controller.*;

public class Image implements ObservableIF {
	
	private static Image instance = new Image();
	
	private BufferedImage imageModele;
	
	private List<ObserverIF> listObservers = new ArrayList<ObserverIF>();
	
	private Image(){
	}
	
	public static Image getInstance(){
		return instance;
		
	}
	
	public BufferedImage getImage(){
		return this.imageModele;
	}
	
	
	public void setImage(BufferedImage img){
		
		this.imageModele = img;
		this.notifyObservers();
		
	}
	
	public void saveImage(File file){
				
		int h = imageModele.getHeight();
		int l = imageModele.getWidth();
		
		BufferedImage imgToSave = new BufferedImage(l, h, BufferedImage.TYPE_INT_RGB);
		
		int[] rgb = imageModele.getRGB(0, 0, l, h, null, 0, l);
		imgToSave.setRGB(0, 0, l, h, rgb, 0, l);
		
		try {
			
			ImageIO.write(imgToSave, "jpg", file);
		
		} catch (IOException e) {
		
			e.printStackTrace();
		
		}
		
	}
	
	public void loadImage(File file){
		
		try {
			
			this.imageModele = ImageIO.read(file);
			this.notifyObservers();
		
		} catch (IOException e) {
		
			e.printStackTrace();
		
		}
		
	}

	public static BufferedImage imageToBI(java.awt.Image img){
		
	    BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

	    Graphics2D bGr = bimage.createGraphics();
	    bGr.drawImage(img, 0, 0, null);
	    bGr.dispose();

	    return bimage;
		
	}
	
	public void addObserver(ObserverIF obs) {
		
		listObservers.add(obs);
	
	}

	public void removeObserver(ObserverIF obs) {
		
		listObservers.remove(obs);
		
	}

	public void notifyObservers() {
		
		for(ObserverIF observer : listObservers){
			
			observer.update(this.getImage());

		}
		
	}
	
}

