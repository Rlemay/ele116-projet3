/************************************************************************************************
Cours: ELE116
Session : A2016
Groupe: 01
Projet: Laboratoire #3
�tudiant(e)(s) : Olivier Goyette
				 Marc-Andr� Pineault
				 Rapha�l Lemay
				 Fran�ois Doskas-Lambert
				 
Code(s) perm. : GOYO20049004
				PINM07019101
				LEMR17129205
				DOSF16049108
				
Nom du fichier: Save.java
Description du fichier: Commande de sauvegarde
Date cr��: 2016-12-04
*************************************************************************************************/

package model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;

public class Save extends Command {

	private File file;
	
	public Save(File fichier){
		
		this.file = fichier;
		
	}
	
	
	public void execute() {

		Image.getInstance().saveImage(file);

	}

	public void undo() {

		//Laiss� vide
		
	}
}
