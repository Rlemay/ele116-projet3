/************************************************************************************************
Cours: ELE116
Session : A2016
Groupe: 01
Projet: Laboratoire #3
�tudiant(e)(s) : Olivier Goyette
				 Marc-Andr� Pineault
				 Rapha�l Lemay
				 Fran�ois Doskas-Lambert
				 
Code(s) perm. : GOYO20049004
				PINM07019101
				LEMR17129205
				DOSF16049108
				
Nom du fichier: Invoker.java
Description du fichier: Singleton contenant la liste des commandes. G�re l'ajout, 
le retour arri�re et le retour avant

Date cr��: 2016-12-04
*************************************************************************************************/

package model;

import java.util.*;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Invoker {

	final int COM_LINE_SIZE = 10;
	
	private int undoNb;
	
	//seule instance de l'Invoker (Singleton)
	private static Invoker instance = new Invoker();
	
	//Liste de commandes
	private List<Command>listeCommande = new ArrayList<Command>();
	
	private Invoker(){
		this.undoNb = 0;
	}
	
	public static Invoker getInstance(){
		return instance;
		
	}
	
	//Ajoute une commande dans la liste et r�cup�re cette commande
	public void addCommand(Command command){
		for(int i=undoNb;i>0;i--){	//on efface les commande qui sont undo
			listeCommande.remove(listeCommande.size()-1);
		}
		if (listeCommande.size() >= COM_LINE_SIZE){	//taille max file
			listeCommande.remove(0);
		}
		listeCommande.add(command);
		undoNb = 0;
		this.retrieveCommand();
	}
	
	//M�thode de r�cup�ration de commande
	public void retrieveCommand(){
		
		this.listeCommande.get(this.listeCommande.size()-1).execute();
	}
	
	//M�thode de retour arri�re
	public void undo(){
		this.listeCommande.get((listeCommande.size()-1)-(undoNb++)).undo();
	}
	
	//M�thode de retour avant
	public void redo(){
		if (undoNb > 0)
			this.listeCommande.get(listeCommande.size()-(undoNb--)).execute();
	}
}
