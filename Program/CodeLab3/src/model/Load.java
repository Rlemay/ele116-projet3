/************************************************************************************************
Cours: ELE116
Session : A2016
Groupe: 01
Projet: Laboratoire #3
�tudiant(e)(s) : Olivier Goyette
				 Marc-Andr� Pineault
				 Rapha�l Lemay
				 Fran�ois Doskas-Lambert
				 
Code(s) perm. : GOYO20049004
				PINM07019101
				LEMR17129205
				DOSF16049108
				
Nom du fichier: Load.java
Description du fichier: Commande de chargement d'image
Date cr��: 2016-12-04
*************************************************************************************************/

package model;

import java.io.File;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Load extends Command {

	public File file;
	
	public void execute() {
	
		Image.getInstance().loadImage(file);
		
	}

	public void undo() {
		
		//Laiss� vide
		
	}

}
