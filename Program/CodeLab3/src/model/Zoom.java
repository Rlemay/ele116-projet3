/************************************************************************************************
Cours: ELE116
Session : A2016
Groupe: 01
Projet: Laboratoire #3
�tudiant(e)(s) : Olivier Goyette
				 Marc-Andr� Pineault
				 Rapha�l Lemay
				 Fran�ois Doskas-Lambert
				 
Code(s) perm. : GOYO20049004
				PINM07019101
				LEMR17129205
				DOSF16049108
				
Nom du fichier: Zoom.java
Description du fichier: Commande de zoom
Date cr��: 2016-12-04
*************************************************************************************************/

package model;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.lang.Math;

import view.MainFen;

public class Zoom extends Command {
	
	private double zoomPourcent;
	BufferedImage imgIn = null;
	BufferedImage imgOut = null;
	
	//R�cup�re les dimensions de l'image dans le mod�le
	private double h_in = Image.getInstance().getImage().getHeight();
	private double l_in = Image.getInstance().getImage().getWidth();
	
	private int h_out = 0;
	private int l_out = 0;
	
	public Zoom(int zoomPourcentage){
		this.zoomPourcent = zoomPourcentage;
	}


	public void execute() {
		
		//Calcul les dimension avec le facteur de zoom
		h_out = (int) (h_in * (zoomPourcent/100));
		l_out = (int) (l_in * (zoomPourcent/100));
		
		imgIn = Image.getInstance().getImage();
		imgOut = new BufferedImage(l_out, h_out, BufferedImage.TYPE_INT_RGB);

		//Utilisation de la m�thode de conversion Image->BufferedImage
		imgOut = Image.imageToBI(imgIn.getScaledInstance(l_out, h_out, imgIn.SCALE_SMOOTH));
		
		//Changement du zoom cummulatif de la MainFen
		MainFen.modifZoom((int)zoomPourcent);
		
		Image.getInstance().setImage(imgOut);
		
	}
	
	//M�me chose qu'avec l'execute mais divise au lieu de multiplier le facteur
	public void undo() {
		
		h_out = (int) (h_in / (zoomPourcent/100));
		l_out = (int) (l_in / (zoomPourcent/100));
		
		imgIn = Image.getInstance().getImage();
		imgOut = new BufferedImage(l_out, h_out, BufferedImage.TYPE_INT_RGB);

		imgOut = Image.imageToBI(imgIn.getScaledInstance(l_out, h_out, imgIn.SCALE_SMOOTH));
		
		MainFen.modifZoom((int)((Math.abs(zoomPourcent)-100)*2));
		
		Image.getInstance().setImage(imgOut);
		
	}


}
