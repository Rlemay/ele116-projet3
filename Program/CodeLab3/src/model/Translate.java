/************************************************************************************************
Cours: ELE116
Session : A2016
Groupe: 01
Projet: Laboratoire #3
�tudiant(e)(s) : Olivier Goyette
				 Marc-Andr� Pineault
				 Rapha�l Lemay
				 Fran�ois Doskas-Lambert
				 
Code(s) perm. : GOYO20049004
				PINM07019101
				LEMR17129205
				DOSF16049108
				
Nom du fichier: Translate.java
Description du fichier: Commande de translation
Date cr��: 2016-12-04
*************************************************************************************************/

package model;

import java.awt.image.BufferedImage;

import view.MainFen;

public class Translate extends Command {

	private int transX;
	private int transY;
	private int transXactu;
	private int transYactu;
	
	BufferedImage imgTemp;
	
	public Translate(int transX, int transY){
		this.transX = transX;
		this.transY = transY;

	}

	//Modifie la valeur de translation de l'image
	public void execute() {
		
		MainFen.modifTranslation(transX, transY);
		Image.getInstance().notifyObservers();

	}

	//Comme l'execute, mais utilise la valeur inverse pour annuler
	public void undo() {

		MainFen.modifTranslation(-transX, -transY);
		Image.getInstance().notifyObservers();
		
	}
}
