/************************************************************************************************
Cours: ELE116
Session : A2016
Groupe: 01
Projet: Laboratoire #3
�tudiant(e)(s) : Olivier Goyette
				 Marc-Andr� Pineault
				 Rapha�l Lemay
				 Fran�ois Doskas-Lambert
				 
Code(s) perm. : GOYO20049004
				PINM07019101
				LEMR17129205
				DOSF16049108
				
Nom du fichier: Lab3.java
Description du fichier: Point d'entr�e du programme, instancie la MainFen et les structures de donn�es
Date cr��: 2016-12-04
*************************************************************************************************/

package lab3;

import view.*;
import controller.*;
import model.*;

public class Lab3 {

	public static void main(String[] args){
		
		Fenetre f = new MainFen(1000, 1000, "Lab3-MainFenetre");
		Image imgModel = Image.getInstance();
		Invoker invoker = Invoker.getInstance();
		
	}
	
}