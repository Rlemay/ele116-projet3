/************************************************************************************************
Cours: ELE116
Session : A2016
Groupe: 01
Projet: Laboratoire #3
�tudiant(e)(s) : Olivier Goyette
				 Marc-Andr� Pineault
				 Rapha�l Lemay
				 Fran�ois Doskas-Lambert
				 
Code(s) perm. : GOYO20049004
				PINM07019101
				LEMR17129205
				DOSF16049108
				
Nom du fichier: LoadMenuItem.java
Description du fichier: MenuItem qui lance la m�thode de chargement de fichier
Date cr��: 2016-12-04
*************************************************************************************************/

package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.*;

import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import model.*;

public class LoadMenuItem extends JMenuItem implements ActionListener {

	Invoker invoker = Invoker.getInstance();

	public void actionPerformed(ActionEvent e) {

		Load loadComm = new Load();
		loadComm.file = MainFen.loadMethod();
		this.invoker.addCommand(loadComm);

	}

}
