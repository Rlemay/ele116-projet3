/************************************************************************************************
Cours: ELE116
Session : A2016
Groupe: 01
Projet: Laboratoire #3
�tudiant(e)(s) : Olivier Goyette
				 Marc-Andr� Pineault
				 Rapha�l Lemay
				 Fran�ois Doskas-Lambert
				 
Code(s) perm. : GOYO20049004
				PINM07019101
				LEMR17129205
				DOSF16049108
				
Nom du fichier: ZoomInButton.java
Description du fichier: Bouton qui envoie une commande d'agrandissement
Date cr��: 2016-12-04
*************************************************************************************************/

package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import model.Image;
import model.Invoker;
import model.Zoom;

public class ZoomInButton extends JButton implements ActionListener{

	Invoker invoker = Invoker.getInstance();
	
	public void actionPerformed(ActionEvent arg0) {	
		this.invoker.addCommand(new Zoom(110));
	}

}
