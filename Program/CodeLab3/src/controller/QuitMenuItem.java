/************************************************************************************************
Cours: ELE116
Session : A2016
Groupe: 01
Projet: Laboratoire #3
�tudiant(e)(s) : Olivier Goyette
				 Marc-Andr� Pineault
				 Rapha�l Lemay
				 Fran�ois Doskas-Lambert
				 
Code(s) perm. : GOYO20049004
				PINM07019101
				LEMR17129205
				DOSF16049108
				
Nom du fichier: QuitMenuItem.java
Description du fichier: MenuItem qui d�clenche la fermeture du programme
Date cr��: 2016-12-04
*************************************************************************************************/

package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;

public class QuitMenuItem extends JMenuItem implements ActionListener{

	public void actionPerformed(ActionEvent e) {

		System.exit(0);
		
	}

}
