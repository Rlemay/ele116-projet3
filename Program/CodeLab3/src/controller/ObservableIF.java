/************************************************************************************************
Cours: ELE116
Session : A2016
Groupe: 01
Projet: Laboratoire #3
�tudiant(e)(s) : Olivier Goyette
				 Marc-Andr� Pineault
				 Rapha�l Lemay
				 Fran�ois Doskas-Lambert
				 
Code(s) perm. : GOYO20049004
				PINM07019101
				LEMR17129205
				DOSF16049108
				
Nom du fichier: ObservableIF.java
Description du fichier: Interface de du sujet (Image)
Date cr��: 2016-12-04
*************************************************************************************************/

package controller;

public interface ObservableIF {
	
	void addObserver(ObserverIF obs);
		
	void removeObserver(ObserverIF obs);
	
	void notifyObservers();
}
