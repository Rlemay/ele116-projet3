/************************************************************************************************
Cours: ELE116
Session : A2016
Groupe: 01
Projet: Laboratoire #3
�tudiant(e)(s) : Olivier Goyette
				 Marc-Andr� Pineault
				 Rapha�l Lemay
				 Fran�ois Doskas-Lambert
				 
Code(s) perm. : GOYO20049004
				PINM07019101
				LEMR17129205
				DOSF16049108
				
Nom du fichier: Fenetre.java
Description du fichier: Classe parente des fen�tres
Date cr��: 2016-12-04
*************************************************************************************************/

package view;

import java.awt.*;

import java.awt.event.*;

import javax.swing.*;

import java.io.File;

import java.io.IOException;

import java.awt.event.*;

public abstract class Fenetre extends JFrame {

	int hauteur = 0;
	int largeur = 0;
	String titre = new String();
	
	Fenetre(int hauteur, int largeur, String titre){
		
		this.hauteur = hauteur;
		this.largeur = largeur;
		this.titre = titre;
		
		this.setSize(largeur, hauteur);
		this.setTitle(titre);
		
	}
	
}
