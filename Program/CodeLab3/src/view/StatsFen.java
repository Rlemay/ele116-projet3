/************************************************************************************************
Cours: ELE116
Session : A2016
Groupe: 01
Projet: Laboratoire #3
�tudiant(e)(s) : Olivier Goyette
				 Marc-Andr� Pineault
				 Rapha�l Lemay
				 Fran�ois Doskas-Lambert
				 
Code(s) perm. : GOYO20049004
				PINM07019101
				LEMR17129205
				DOSF16049108
				
Nom du fichier: StatsFen.java
Description du fichier: Fen�tre d'affichage de statistiques
Date cr��: 2016-12-04
*************************************************************************************************/

package view;

import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class StatsFen extends Fenetre {

	private JPanel panel = new JPanel();
	private JLabel label = new JLabel();
	
	private int offsetX = 0;
	private int offsetY = 0;
	private int hauteur = 0;
	private int largeur = 0;
	private int zoom = 0;
	
	protected StatsFen(int hauteur, int largeur, String titre) {

		//Configurations de la fen�tre
		super(hauteur, largeur, titre);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setLocation(0, 505);
		this.add(panel);
		panel.setLayout(new BorderLayout());
		panel.add(label, BorderLayout.CENTER);
		
		label.setHorizontalAlignment((int) label.CENTER_ALIGNMENT);
		label.setFont(new Font("Serif", Font.PLAIN, 24));
		
	}
	
	/*****************************************************
	 M�thode de mise � jour de l'affichage, prend les
	 valeurs en param�tre et les affiches dans un JLabel
	 *****************************************************/
	protected void updateDisplay(int translationX, int translationY, int hauteurImage, int largeurImage, int facteurZoom){
		
		String temp = new String();
		
		this.offsetX = translationX;
		this.offsetY = translationY;
		this.hauteur = hauteurImage;
		this.largeur = largeurImage;
		this.zoom = facteurZoom;
		
		temp = "Offset en X : " + offsetX + "\n\nOffset en Y : " + offsetY + "\n\nHauteur : " 
				+ hauteur + " pixels" + "\n\nLargeur : " + largeur + " pixels" +
				" degr�s" + "\n\nFacteur de zoom : " + zoom + " %";
		
		label.setText(toMultiLines(temp));

	}
	
	private String toMultiLines(String input){
		
		return "<html>" + input.replaceAll("\n", "<br>") + "</html>";
		
	}

}
