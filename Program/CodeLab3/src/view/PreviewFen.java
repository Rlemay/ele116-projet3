/************************************************************************************************
Cours: ELE116
Session : A2016
Groupe: 01
Projet: Laboratoire #3
�tudiant(e)(s) : Olivier Goyette
				 Marc-Andr� Pineault
				 Rapha�l Lemay
				 Fran�ois Doskas-Lambert
				 
Code(s) perm. : GOYO20049004
				PINM07019101
				LEMR17129205
				DOSF16049108
				
Nom du fichier: PreviewFen.java
Description du fichier: Fen�tre d'affichage de l'image en version r�duite
Date cr��: 2016-12-04
*************************************************************************************************/

package view;

import java.awt.*;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class PreviewFen extends Fenetre {

	private JPanel panel = new JPanel();
	private JLabel label = new JLabel();
	
	protected PreviewFen(int hauteur, int largeur, String titre) {
		
		//Configurations de la fen�tre
		super(hauteur, largeur, titre);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setResizable(false);
		this.add(panel);
		panel.setLayout(new BorderLayout());
		panel.add(label, BorderLayout.CENTER);
		
	}


	/**********************************************
	 M�thode d'affichage du thumbnail, calcul le
	 ratio de l'image, donne une grandeur fixe � la
	 hauteur et calcule la nouvelle largeur selon 
	 le ratio. Scale ensuite l'image aux nouvelles
	 grandeurs et l'affiche
	 **********************************************/
	protected void afficheImage(int hauteur, int largeur, BufferedImage image){
		
		double ratio = 0;
		int hauteurCalc = 0;
		int largeurCalc = 0;
		
		ratio = (double) largeur/hauteur;
		hauteurCalc = 300;
		largeurCalc = (int) (ratio * hauteurCalc);

		label.setIcon(new ImageIcon(image.getScaledInstance(largeurCalc, hauteurCalc, image.SCALE_SMOOTH)));
		this.setBounds(this.getX(), this.getY(), largeurCalc, hauteurCalc);
		panel.repaint();
		
	}
	
}
