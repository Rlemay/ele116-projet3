/************************************************************************************************
Cours: ELE116
Session : A2016
Groupe: 01
Projet: Laboratoire #3
�tudiant(e)(s) : Olivier Goyette
				 Marc-Andr� Pineault
				 Rapha�l Lemay
				 Fran�ois Doskas-Lambert
				 
Code(s) perm. : GOYO20049004
				PINM07019101
				LEMR17129205
				DOSF16049108
				
Nom du fichier: MainFen.java
Description du fichier: G�re l'affiche et le control de la GUI
Date cr��: 2016-12-04
*************************************************************************************************/

package view;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.ImageOutputStream;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;
import java.awt.event.*;

import controller.*;
import model.*;
import model.Image;

public class MainFen extends Fenetre implements ObserverIF {
	
	//Image qui sera utilis�e pour la fenetre de preview
	public static BufferedImage imageEnInput;

	//Instances des fenetres de statistiques et de preview
	private static PreviewFen prevFen = new PreviewFen(0, 0, "Lab3 - Image preview");
	private static StatsFen statFen = new StatsFen(500, 400, "Lab3 - modification tracker");
	
	//H�rite d'un JLabel, vas contenir l'image � afficher
	private static DisplayCentral canvas = new DisplayCentral();
	
	//valeurs d'offset de l'image dans la fenetre
	private static int offsetX = 0;
	private static int offsetY = 0;
	
	//position centrale dans l'affichage
	private static final int milieuOriginalX = 492;
	private static final int milieuOriginalY = 451;
	
	//Niveau initial de zoom
	private static double pourcentZoom = 100;
	
	/*******************************
	Structure de la fenetre :
	
		Fenetre
		|	menuBar
		|	|
		|	Centre
		|	|	toolBar
		|	|	|
		|	|	ScrollPane
		|	|	|	zoneTravail
		|	|	|	|	canvas
	
	********************************/
	
	//D�claration des diff�rents composants de la fen�tre
	JPanel centre = new JPanel();
	JPanel toolBar = new JPanel();
	JPanel zoneTravail = new JPanel();
	JScrollPane scrollPane = new JScrollPane(zoneTravail);
	
	//-----------------------------------
	//D�claration de la barre de menu, des menus et des MenuItems
	//-----------------------------------
	JMenuBar menuBar = new JMenuBar();
		JMenu mFichier = new JMenu("Fichier");
			JMenuItem miCharger = new JMenuItem("Charger une image");
			JMenuItem miSave = new JMenuItem("Enregistrer");
			JMenuItem miFermer = new JMenuItem("Fermer");
			
		JMenu mEditer = new JMenu("Editer");
			JMenuItem miUndo  = new JMenuItem("Retour arri�re");
			JMenuItem miRedo = new JMenuItem("Retour avant");
	
		JMenu mApropos = new JMenu("� propos");
			JMenuItem miAuteurs = new JMenuItem("Auteurs");
	//-----------------------------------
			
	//D�claration des boutons de translation
	JButton upButton = new JButton("Translate haut");
	JButton downButton = new JButton("Translate bas");
	JButton leftButton = new JButton("Translate gauche");
	JButton rightButton = new JButton("Translate droite");
	
	//D�claration des bouton de zoom
	JButton zoomIN = new JButton("Zoom in");
	JButton zoomOUT = new JButton("Zoom out");
	
	public MainFen(int hauteur, int largeur, String titre) {
		
		super(hauteur, largeur, titre);
		
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setResizable(false);
		
		//-----------------------------------
		//Ajout des Menus et MenuItems dans la barre de menu
		//-----------------------------------
		menuBar.add(mFichier);
			mFichier.add(miCharger);
			mFichier.add(miSave);
			mFichier.add(miFermer);
		
		menuBar.add(mEditer);
			mEditer.add(miUndo);
			mEditer.add(miRedo);
		
		menuBar.add(mApropos);
			mApropos.add(miAuteurs);
		//-----------------------------------
			
		//Ajout des bouton dans le panel toolBar
		toolBar.add(upButton);
		toolBar.add(downButton);
		toolBar.add(leftButton);
		toolBar.add(rightButton);
		toolBar.add(zoomIN);
		toolBar.add(zoomOUT);
		
		//-----------------------------------
		//Ajout des listeners custom du controlleur
		//aux diff�rents controles de la fenetre
		//-----------------------------------
		miCharger.addActionListener(new LoadMenuItem());
		miSave.addActionListener(new SaveMenuItem());
		miFermer.addActionListener(new QuitMenuItem());
		
		miUndo.addActionListener(new UndoMenuItem());
		miRedo.addActionListener(new RedoMenuItem());
		
		miAuteurs.addActionListener(new AuteursMenuItem());
			
		upButton.addActionListener(new UpButton());
		downButton.addActionListener(new DownButton());
		leftButton.addActionListener(new LeftButton());
		rightButton.addActionListener(new RightButton());
		
		zoomIN.addActionListener(new ZoomInButton());
		zoomOUT.addActionListener(new ZoomOutButton());
		//-----------------------------------
		
		//Ajustements fait au panel zoneTravail
		zoneTravail.setLayout(null);
		zoneTravail.setSize(milieuOriginalX * 2, milieuOriginalY * 2);
		zoneTravail.add(canvas);
		zoneTravail.setBackground(Color.WHITE);

		//Ajout des panneaux principaux � la fenetre
		this.setLocationRelativeTo(null);	
		this.setLayout(new BorderLayout());
		this.add(menuBar, BorderLayout.NORTH);
		this.add(centre, BorderLayout.CENTER);
		
		//Config. des diff�rents layouts
		centre.setLayout(new BorderLayout());
		toolBar.setLayout(new FlowLayout());
		centre.add(toolBar, BorderLayout.NORTH);
		centre.add(scrollPane, BorderLayout.CENTER);
		
		//Ajout d'une bordure grise
		scrollPane.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, Color.gray));
		
		//Affichage de la GUI
		this.setVisible(true);

		//-----------------------------------
		//Ajout de l'observer sur le mod�le
		//-----------------------------------
		Image.getInstance().addObserver(this);
		//-----------------------------------
	
	}
	
	//Classe de l'instance nomm�e canvas
	private static class DisplayCentral extends JLabel{
		
		private BufferedImage img = null;
		
		protected DisplayCentral(){
			
			this.setBackground(Color.WHITE);
			this.setOpaque(true);
			
		}
			
	}
	
	/***********************************************************
	M�thode de chargement, retourne un fichier en param�tre pour
	qu'il soit charg� par le mod�le plus tard. Contient aussi
	la mise � jour des fen�tres de statistique et de preview.
	***********************************************************/
	public static File loadMethod(){
		
		int returnVal = 0;
		
		File loadedFile = new File("LoadedFile");
		JFileChooser fileChooser = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("JPG and PNG images only", "jpg", "png");
		fileChooser.setFileFilter(filter);
		
		int hauteurImage = 0;
		int largeurImage = 0;
		
		returnVal = fileChooser.showOpenDialog(new JFrame());
		
		if(returnVal == JFileChooser.APPROVE_OPTION){
			
			loadedFile = fileChooser.getSelectedFile();
			
			try{
				
				imageEnInput = ImageIO.read(loadedFile);
				
			}
			catch(Exception e){
				
				e.printStackTrace();
				
			}
			
			hauteurImage = imageEnInput.getHeight();
			largeurImage = imageEnInput.getWidth();
			
			//Mise � jour fen�tres de preview et stats
			prevFen.afficheImage(hauteurImage, largeurImage, imageEnInput);
			statFen.updateDisplay(0, 0, hauteurImage, largeurImage, 1);
			prevFen.setVisible(true);
			statFen.setVisible(true);
					
		}
		
		return loadedFile;
		
	}
	
	/********************************************************
	Fonction lanc�e lors de la notification de changement du
	mod�le. Re�ois l'image � jour en param�tre et la positionne
	� l'endroit appropri�. Update aussi la fen�tre de stats.	 
	*********************************************************/
	public void update(BufferedImage img) {
		
		int largeurImg;
		int hauteurImg;
		
		largeurImg = img.getWidth();
		hauteurImg = img.getHeight();
		
		//Mise � jour de l'image � afficher
		canvas.setSize(largeurImg, hauteurImg);
		canvas.setIcon(new ImageIcon(img));

		//Recentrage de l'image
		canvas.setBounds(milieuOriginalX - (int)(largeurImg/2) + offsetX, 
							milieuOriginalY - (int)(hauteurImg/2) + offsetY,
								largeurImg, hauteurImg);
		
		
		//Logique d'affichage si l'image est plus grand ou plus petit que la fen�tre
		if((largeurImg > milieuOriginalX * 2) || (hauteurImg > milieuOriginalY * 2)){
			
			scrollPane.setViewportView(canvas);
			
		}
		else{
			
			scrollPane.setViewportView(zoneTravail);
			zoneTravail.add(canvas);
			
		}

		//Mise � jour de la fen�tre de statistiques
		statFen.updateDisplay(offsetX, offsetY, hauteurImg, largeurImg, (int) MainFen.pourcentZoom);
		
	}
	
	/**********************************************
	M�thode de sauvegarde, affiche le prompt de
	JFileChooser et retourne le file obtenu.
	**********************************************/
	public static File saveMethod(){
		
		JFileChooser fileSaver = new JFileChooser();
		FileNameExtensionFilter saveFilter = new FileNameExtensionFilter("JPEG only", "jpg");
		fileSaver.setFileFilter(saveFilter);
		File f = null;
		
		if(fileSaver.showSaveDialog(new JFrame()) == fileSaver.APPROVE_OPTION){
			
			return f = new File(fileSaver.getSelectedFile().getAbsolutePath() + ".jpg");
			
		}
		
		return null;
		
	}
		
	//M�thode setter pour la valeur de zoom cummulative
	public static void modifZoom(double pourcentageZoom){
		
		MainFen.pourcentZoom = MainFen.pourcentZoom * (pourcentageZoom/100);
		
	}
	
	//m�thode setter pour la valeur de translation cumulative
	public static void modifTranslation(int transX, int transY){
		
		MainFen.offsetX = MainFen.offsetX + transX;
		MainFen.offsetY = MainFen.offsetY + transY;
		
	}

}
